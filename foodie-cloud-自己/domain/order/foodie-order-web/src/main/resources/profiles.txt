profiles 多环境梳理
1.数据源配置
    1.1 url 根据自身情况修改为localhost 或者 内网ip（集群或者分布式系统一定要使用内网ip）
    1.2 密码 123456 改为自己的密码 123456
2.mybatis 日志打印
    dev环境可以答应
    test测试环境可以答应
    prod生产环境不需要答应
3.图片保存目录 和 图片服务请求路径配置
    #生产环境
    #Windows系统要用两个反斜杠：\\
    file.imageUserFaceLocation:\\workspaces/images/foodie/faces
    #图片服务地址，Linux中的图片服务地址
    file.imageServerUrl:http://192.168.40.128:8088/foodie-dev-api/foodie/faces
4.从支付中心回调天天吃货后端服务的回调地址
    http://192.168.40.128:8088/foodie-dev-api/orders/notifyMerchantOrderPaid

打包方式：
1.jar
    服务化的概念，后续会接触SpringCloud，所有的服务打包 都是以jar的形式存在的
2.war
    应用程序的概念，也可以向外提供服务和借口